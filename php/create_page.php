<?
require('include/auth.php');
if (!is_authenticated()) {
    show_auth_page();
}
require('include/page_management.php');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (!isset($_FILES) || !isset($_FILES['page'])) {
        $error = 'No file provided.';
    }
    $error = create_page($_POST['name']);
    if ($error === null) {
        $success = "Successfully added page";
    }
}
?>
<html>
    <head>
        <title><?=$website_name?></title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="new-page-parent">
            <h1>Add Page</h1>
            <hr>
            <a href="admin.php">Admin</a>
            <form class="new-page-form" method="post" enctype="multipart/form-data">
                <input class="text-input new-page-input" placeholder="Page Name" name="name" type="text"/>
                <input class="new-page-file" id="page-upload" name="page" type="file"/>
                <input class="button-input" value="Create Page" type="submit"/>
            </form>
            <? if ($error !== null) echo('<p class="new-page-error fade-out-5s">' . $error . "</p>"); ?>
            <? if ($success !== null) echo('<p class="new-page-success fade-out-5s">' . $success . "</p>"); ?>
        </div>
    </body>
</html>
