<?
require('include/page_management.php');
$page = get_page($_GET['page']);
$error = null;
if ($page === null) {
    $error = "Invalid page.";
}
?>
<html>
    <head>
        <title><?=$website_name?></title>
        <link rel="stylesheet" href="style.css">
        <? if ($error === null) { ?>
        <style>html { background: url(<?=get_page_link($page)?>) no-repeat center center fixed; background-size: contain; }</style>
        <? } else { ?>
        <div><?=$error ?></div>
        <? } ?>
    </head>
    <body>
        <h1><?=$page['name']?></h1>
    </body>
</html>
