<?php
require('include/auth.php');
if (!is_authenticated()) {
    show_auth_page();
}
require('include/settings.php');
require('include/admin.php');
require('include/page_management.php');
?>

<html>
    <head>
        <title><?=$website_name?></title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body class="centered-page">
        <div class="parent">
            <h1>
                Administration
            </h1>
        </div>
        <?=show_all_pages()?>
        <div>
            <a href="create_page.php"/>Create new page</a>
        </div>
    </body>
</html>
