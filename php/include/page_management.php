<?php

require('include/database.php');
require('include/settings.php');

function get_page_file($file_num, $extension) {
    global $deploy_path;
    return "/php$deploy_path/images/$file_num.$extension";
}

function get_page_link($page) {
    return "images/$page[file].$page[extension]";
}

function get_pages() {
    $db = get_connection();
    $result = $db->query('select name, file from page;');
    return $result;
}

function show_all_pages() {
    echo '<div class="pages"><h3>Pages</h3><hr>';
    foreach (get_pages() as $page) {
        require('include/page_template.php');
    }
    echo '</div>';
}

function upload_error_code_to_string($code)
{
    switch ($code) {
    case UPLOAD_ERR_OK: return null;
    case UPLOAD_ERR_INI_SIZE: return "The uploaded file exceeds the upload_max_filesize directive in php.ini";
    case UPLOAD_ERR_FORM_SIZE: return "The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form";
    case UPLOAD_ERR_PARTIAL: return "The uploaded file was only partially uploaded";
    case UPLOAD_ERR_NO_FILE: return "No file was uploaded";
    case UPLOAD_ERR_NO_TMP_DIR: return "Missing a temporary folder";
    case UPLOAD_ERR_CANT_WRITE: return "Failed to write file to disk";
    case UPLOAD_ERR_EXTENSION: return "File upload stopped by extension";
    default: return "Unknown Error";
    }
}

function create_page($name) {
    if ($name === null || strlen($name) === 0) {
        return "Name required";
    }
    $error = upload_error_code_to_string($_FILES['page']['error']);
    if ($error !== null) {
        return $error;
    }
    if (!is_uploaded_file($_FILES['page']['tmp_name'])) {
        error_log("File " . $_FILES['page']['tmp_name'] . " is not uploaded? Trying to trick me!?\n");
        return "Why u try to trick me?";
    }
    $originalExtension = pathinfo($_FILES['page']['name'], PATHINFO_EXTENSION);
    /* Whitelist for file extensions. */
    if ($originalExtension === null || !preg_match('/^[a-zA-Z0-9_][a-zA-Z0-9_]?[a-zA-Z0-9_]?$/', $originalExtension)) {
        return "Invalid Extension. The extension must be 1-3 characters, numbers, letters, and underscore only.";
    }
    if (null === ($db = get_connection())) {
        return "Internal Error Connecting";
    }
    $file_number = $db->query("select max(file) from page;");
    $fetch = $file_number->fetchColumn();
    if ($fetch === null) {
        $file_number = 0;
    } else {
        $file_number = $fetch + 1;
    }
    $new_file = get_page_file($file_number, $originalExtension);
    error_log("Saving file to $new_file\n");
    if (file_exists($new_file)) {
        error_log("File $new_file already exists!\n");
        return "Internal Error Preparing";
    }
    if (!move_uploaded_file($_FILES['page']['tmp_name'], "$new_file")) {
        return 'Internal Error Saving';
    }
    $st = $db->prepare("insert into page (name, file, extension) values (?, ?, ?);");
    if (!$st->execute(array($name, $file_number, $originalExtension))) {
        if (!unlink("$new_file")) {
            error_log("Error deleting $new_file .\n");
        }
        error_log("Error executing query. deleted new file $new_file\n");
        return 'Internal Error Writing';
    }
    return null;
}

function edit_page($page_data) {
    if ($page_data['pagenum'] === null || strlen($page_data['pagenum']) === 0) {
        return 'Invalid page to edit';
    }
    $old_page = get_page($page_data['pagenum']);
    if ($old_page === null) {
        return "Invalid page to edit";
    }
    if (null === ($db = get_connection())) {
        return "Internal Error Connecting";
    }
    $new_file=$old_page['file'];
    $new_extension=$old_page['extension'];
    if (isset($_FILES) && isset($_FILES['page']) && $_FILES['page']['error'] !== 4) {
        $error = upload_error_code_to_string($_FILES['page']['error']);
        if ($error !== null) {
            return $error;
        }
        if (!is_uploaded_file($_FILES['page']['tmp_name'])) {
            error_log("File " . $_FILES['page']['tmp_name'] . " is not uploaded? Trying to trick me!?\n");
            return "Why u try to trick me?";
        }
        $originalExtension = pathinfo($_FILES['page']['name'], PATHINFO_EXTENSION);
        /* Whitelist for file extensions. */
        if ($originalExtension === null || !preg_match('/^[a-zA-Z0-9_][a-zA-Z0-9_]?[a-zA-Z0-9_]?$/', $originalExtension)) {
            return "Invalid Extension. The extension must be 1-3 characters, numbers, letters, and underscore only.";
        }
        $file_number = $db->query("select max(file) from page;");
        $new_extension = $originalExtepnsion;
        $new_file=$file_number;
        $fetch = $file_number->fetchColumn();
        if ($fetch === null) {
            $file_number = 0;
        } else {
            $file_number = $fetch + 1;
        }
        $new_file = get_page_file($file_number,$originalExtension);
        error_log("Saving new edit file to $new_file\n");
        if (file_exists($new_file)) {
            error_log("File $new_file already exists!\n");
            return "Internal Error Preparing";
        }
        if (!move_uploaded_file($_FILES['page']['tmp_name'], "$new_file")) {
            return 'Internal Error Saving';
        }
        if (!unlink(get_page_file($old_page['file'], $old_page['extension']))) {
            error_log("Problem deleting old file $old_page[file].$old_page[extension]          Continuing...\n");
        }
    }
    $st = $db->prepare("update page set name=?, file=?, extension=? where file=?;");
    if (!$st->execute(array($page_data['name'], $new_file, $new_extension, $old_page['file']))) {
        if (!unlink("$new_file")) {
            error_log("Error deleting $new_file in edit.\n");
        }
        error_log("Error executing query. deleted new file $new_file\n");
        return 'Internal Error Writing';
    }
    return null;
}

function get_page($page_num) {
    if (null === ($db = get_connection())) {
        return null;
    }
    $st = $db->prepare("select name, file, extension from page where file = ?;");
    if (!$st->execute(array($page_num))) {
        error_log("Failed to execute query on page num $page_num\n");
        return null;
    }
    $ret = $st->fetchAll();
    if (!array_key_exists('0', $ret)) {
        return null;
    }
    return $ret['0'];
}
?>
