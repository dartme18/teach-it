<?
require('include/auth.php');
if (!is_authenticated()) {
    show_auth_page();
}
require('include/page_management.php');
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $error = edit_page($_POST);
    if ($error === null) {
        $success = "Successfully saved page";
    }
    $page = get_page($_POST['pagenum']);
} else {
    $page = get_page($_GET['page']);
}
if ($page === null) {
    $error = "Invalid page";
}
?>
<html>
    <head>
        <title><?=$website_name?></title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div class="new-page-parent">
            <h1>Edit Page</h1>
            <hr>
            <a href="admin.php">Admin</a>
            <form class="new-page-form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="pagenum" value="<?=$page['file']?>">
                <input class="text-input new-page-input" placeholder="Page Name" name="name" type="text" value="<?=$page['name']?>">
                <input class="new-page-file" id="page-upload" name="page" type="file"/>
                <input class="button-input" value="Save" type="submit"/>
                <img src="<?=get_page_link($page)?>" class="edit-view-image"/>
            </form>
            <? if ($error !== null) echo('<p class="new-page-error">' . $error . "</p>"); ?>
            <? if ($success !== null) echo('<p class="new-page-success">' . $success . "</p>"); ?>
        </div>
    </body>
</html>
